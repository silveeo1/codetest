<?php

namespace App\Http\Livewire;

use Livewire\Component;

class SocialFooter extends Component
{
    public function render()
    {
        return view('livewire.social-footer');
    }
}
