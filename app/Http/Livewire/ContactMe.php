<?php

namespace App\Http\Livewire;

use Livewire\Component;

class ContactMe extends Component
{
    public $name;
    public $email;
    public $question;
    public $response;

    protected $rules = [
        'name' => 'required|min:6',
        'email' => 'required|email',
        'question' => 'required|min:5',
    ];

    protected $messages = [
        'question.required' => 'The Message box cannot be empty.',
        /* 'email.email' => 'The Email Address format is not valid.', */
    ];

    public function submit()
    {
        $validatedData = $this->validate();
        dd('This submit was a a success');


    }

    public function render()
    {
        return view('livewire.contact-me');
    }
}
