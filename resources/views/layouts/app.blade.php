<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Code Test</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Fondamento&display=swap" rel="stylesheet">

        <!-- Styles -->
        <link rel="stylesheet" href="css/main.css">
        @livewireStyles
    </head>

    <body>

        @yield('body-content')

        @livewireScripts

    </body>

</html>
