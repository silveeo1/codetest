@extends( 'layouts.app' )

@section('body-content')

<div class="border-b-2 border-black">
    <livewire:about-me>
</div>

<div class="border-b-2 border-black">
    <livewire:contact-me>
</div>

<livewire:social-footer>

@endsection
