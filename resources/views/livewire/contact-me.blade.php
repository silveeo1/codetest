<div class="w-10/12 m-auto">
    <div>

        <h1 class="text-5xl w-10/12 m-auto pb-10 md:w-1/2">Contact Me</h1>

        <form wire:submit.prevent="submit">
            <input type="text" placeholder="Name" wire:model="name" class="block m-auto border-b-2 border-black w-10/12 bg-transparent mb-10 outline-none px-2 md:w-1/2">
            @error('name') <span class="block m-auto w-10/12 px-2 md:w-1/2">{{ $message }}</span> @enderror

            <input type="text" placeholder="Email" wire:model="email" class="block m-auto border-b-2 border-black w-10/12 bg-transparent mb-10 outline-none px-2 md:w-1/2">
            @error('email') <span class="block m-auto w-10/12 px-2 md:w-1/2">{{ $message }}</span> @enderror

            <textarea placeholder="How can I help?" wire:model="question" class="block m-auto border-2 border-black w-10/12 bg-transparent mb-10 outline-none px-2 h-32 md:w-1/2"></textarea>
            @error('question') <span class="block m-auto w-10/12 px-2 md:w-1/2">{{ $message }}</span> @enderror

    {{--  this button styling is not right.  will need to revisit if I have time --}}
            <button class="block m-auto border-2 hover:border-black border-gray-900 hover:font-semibold w-10/12 mb-10 px-2 md:w-1/4">Submit</button>

        </form>
    </div>
</div>
